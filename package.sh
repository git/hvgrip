#!/bin/bash

PACKAGE="grip-3.4.0"

# Create temporary export archive
git archive --format=tar --prefix=${PACKAGE}/ master | bzip2 -9 > ${PACKAGE}-git.tar.bz2

# Generate autoconf files for distribution
tar -xvf ${PACKAGE}-git.tar.bz2
cd ${PACKAGE}
./autogen.sh

# Generate translation files (po)
./configure
make -C po
make distclean

cd ../
tar cvf - ${PACKAGE} | bzip2 -9 > ${PACKAGE}.tar.bz2

# Cleanup
rm -rf ${PACKAGE}
rm -f ${PACKAGE}-git.tar.bz2
