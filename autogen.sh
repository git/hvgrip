#!/bin/sh
# autogen.sh -- Use this script to create/update GNU Build System files from
#               ther GIT distribution.

set -e

# To test that we are really in the top-level directory
TEST_FILE=src/grip.c

test -f ${TEST_FILE} || {
    echo "Error: You must run this script in the top-level project directory"
    exit 1
}

(autoreconf --version) < /dev/null > /dev/null 2>&1 || {
    echo "Error: You must have autoreconf installed to generate the GNU Build System files."
    exit 1
}

# ChangeLog is generated from git log output using "make changelog"
touch ChangeLog

autoreconf -v --install --force

rm -rf autom4te.cache
